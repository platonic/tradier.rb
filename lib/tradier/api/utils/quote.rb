require 'tradier/api/utils/base'

module Tradier
  module API
    module Utils
      class Quote < Tradier::API::Utils::Base

        def body
          if @attrs[:quotes][:quote]
            nested_array(Tradier::Quote, [:quotes, :quote])
          else
            []
          end
        end

        def unmatched_symbols
          if @attrs[:quotes][:unmatched_symbols]
            nested_array(Tradier::UnmatchedSymbol, [:quotes, :unmatched_symbols, :symbol])
          else
            []
          end
        end
      end
    end
  end
end
