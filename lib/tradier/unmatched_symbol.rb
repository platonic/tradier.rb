require 'tradier/base'

module Tradier
  class UnmatchedSymbol < Tradier::Base
    attr_reader :symbol

    def self.from_response(symbol = nil)
      new(symbol: symbol)
    end

    def ==(quote)
      self.symbol == quote.symbol
    end
  end
end
